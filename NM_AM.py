#Pour la répartition du travail: 
# Alois : Creation des différentes class (subdiveMain, AddSubdiv et RemoveSubdiv)
# Alois + Nico : utilisation de class methode et configuration de l'interface
# Nico : mise en place des Poll pour eviter de pouvoir subdivide lorsque l'objet ne peut pas l'être


import bpy, addon_utils, os, re
from bpy.types import Header, Menu, Panel
from bpy.utils import register_class, unregister_class


class SubdivideMain_NM_AM(bpy.types.Panel): #Alois : creation d'une première class pour faire la mise en forme
    bl_label = "Subdivide ON/OFF"
    bl_idname = "SubdivideMain"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Subdivide"
    bl_parent_id = "MCN_PT_Main_Panel"

    def draw(self, context):
        layout = self.layout
        selected = bpy.context.active_object

        if selected and selected.type == 'MESH': # Alois+Nico mise en forme de l'interface


            spl = layout.split(align=True)
            
            spl.scale_y = 2.0
            spl.operator("addon.add_subdivision", text="Add Subdivision", icon='ADD') #creation du bouton subdiv
            spl.operator("addon.remove_subdivision", text="Remove Subdivision", icon='REMOVE') # creation du bouton remove subdiv
      
        else:
            box = layout.box()
            box.label(text='Please select an object', icon='ERROR')


class Add_subdivision_NM_AM(bpy.types.Operator): # creation de la class add subdiv
    bl_idname = "addon.add_subdivision"
    bl_label = "Add Subdivision"
    bl_description = "Add Subdivision modifier of level"

    @classmethod #Nico : utilisation de class methode et de poll pour empecher de pouvoir subdivide si l'objet ne peut pas l'être
    def poll(cls, context):
        if 'Subdivision' not in context.active_object.modifiers:
            return True

    def execute(self, context):
        active_obj = bpy.context.active_object
        if active_obj.type == 'MESH' and 'Subdivision' not in active_obj.modifiers:
            bpy.ops.object.modifier_add(type='SUBSURF') # on ajoute le modif subdiv et on le met à 2
            active_obj.modifiers["Subdivision"].levels = 2
            bpy.ops.object.shade_smooth()# on le shadesmooth 
            self.report({'INFO'}, "Successfully added Subdivision modifier")
            return {'FINISHED'}
        else: # on crée un boucle esle au cas ou ça marche pas 
            self.report({'ERROR'},
                        "Impossible de Subdivide cet objet")
            return {'CANCELLED'}


class Remove_subdivisions_NM_AM(bpy.types.Operator): # creation de la class remove subdiv
    
    bl_idname = "addon.remove_subdivision"
    bl_label = "Remove Subdivision"
    bl_description = "Remove Subdivision modifiers"

    @classmethod #Nico : utilisation de class methode et de poll pour empecher de pouvoir unsubdivide si l'objet ne peut pas l'être
    def poll(cls, context):
        if 'Subdivision' in context.active_object.modifiers:
            return True

    def execute(self, context):
        active_obj = bpy.context.active_object
        for modifier in active_obj.modifiers:
            if modifier.type == 'SUBSURF':
                bpy.ops.object.modifier_remove(modifier=modifier.name) # on remove le modif subdivide
                bpy.ops.object.shade_flat()
        self.report({'INFO'}, "Objet Subdivisé")
        return {'FINISHED'}




def register():
    bpy.utils.register_class(SubdivideMain_NM_AM)
    bpy.utils.register_class(Add_subdivision_NM_AM)
    bpy.utils.register_class(Remove_subdivisions_NM_AM)
    
def unregister():
    bpy.utils.unregister_class(SubdivideMain_NM_AM)
    bpy.utils.unregister_class(Add_subdivision_NM_AM)
    bpy.utils.unregister_class(Remove_subdivisions_NM_AM)

